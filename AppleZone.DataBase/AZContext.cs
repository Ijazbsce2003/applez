﻿using AppleZones.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppleZone.DataBase
{
    public class AZContext : DbContext
    {
        public AZContext() : base("Connectionstring")
        {

        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
