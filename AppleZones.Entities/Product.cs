﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppleZones.Entities
{
    public class Product :BaseClass
    {
        public Category Category { get; set; }
      
        public decimal Price { get; set; }
    }
}
